#include <termios.h>
#include <unistd.h>

#include "editor.h"
#include "screen.h"
#include "term.h"
#include "utils.h"

int main() {
  term_set_raw();
  bool running = true;
  editor_state_t state = editor_init();

  while (running) {
    screen_refresh(&state);
    int k = read_key();
    running = editor_parse_input(&state, k);
  }

  return 0;
}
