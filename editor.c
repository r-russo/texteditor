#include "editor.h"
#include "content.h"
#include "defs.h"
#include "screen.h"
#include "term.h"
#include "text_buffer.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

editor_state_t editor_init() {
  char *buffer = NULL;
  size_t len;
  FILE *f = fopen("editor.c", "r");
  if (f == NULL) {
    error_and_quit("File not found");
  }
  fseek(f, 0, SEEK_END);
  len = ftell(f);
  fseek(f, 0, SEEK_SET);
  buffer = malloc(len * sizeof(char));
  if (!buffer) {
    error_and_quit("Can't allocate read buffer");
  }

  fread(buffer, sizeof(char), len, f);
  fclose(f);

  return (editor_state_t){
      .mode = MODE_NORMAL,

      .cursor_x = 0,
      .cursor_y = 0,
      .screen_width = 0,
      .screen_height = 0,

      .offset_x = 0,
      .offset_y = 0,
      .content = content_init(buffer, len),
  };
}

static void editor_scroll(editor_state_t *s) {
  if (s->cursor_x < s->offset_x) {
    s->offset_x = s->cursor_x;
  } else if (s->cursor_x >= s->offset_x + s->screen_width) {
    s->offset_x = s->cursor_x - s->screen_width + 1;
  }

  int height = s->screen_height - STATUS_BAR_HEIGHT;
  if (s->cursor_y < s->offset_y) {
    s->offset_y = s->cursor_y;
  } else if (s->cursor_y >= s->offset_y + height) {
    s->offset_y = s->cursor_y - height + 1;
  }
}

static void editor_move_cursor(editor_state_t *s, int k) {
  int nrows = s->content.len;
  switch (k) {
  case 'h':
  case ARROW_LEFT:
    s->cursor_x--;
    if (s->cursor_x < 0) {
      s->cursor_x = 0;
    }
    break;
  case 'j':
  case ARROW_DOWN:
    s->cursor_y++;
    if (s->cursor_y >= nrows) {
      s->cursor_y = nrows - 1;
    }
    break;
  case 'k':
  case ARROW_UP:
    s->cursor_y--;
    if (s->cursor_y < 0) {
      s->cursor_y = 0;
    }
    break;
  case 'l':
  case ARROW_RIGHT:
    s->cursor_x++;
    break;
  }

  int ncols = s->content.rows[s->cursor_y].len;
  if (s->cursor_x >= ncols) {
    s->cursor_x = ncols;
  }
}

static void editor_insert_character(editor_state_t *s, char c) {
  text_buffer_t *row = &s->content.rows[s->cursor_y];
  text_buffer_grow(row, 1);

  size_t n = row->len - s->cursor_x;
  memmove(&row->data[s->cursor_x], &row->data[s->cursor_x - 1], n);

  row->data[s->cursor_x++] = c;
}

static void editor_insert_line(editor_state_t *s) {
  content_t *c = &s->content;
  content_grow(c);
  size_t n = c->len - s->cursor_y;
  memmove(&c->rows[s->cursor_y + 1], &c->rows[s->cursor_y], n);
  c->rows[s->cursor_y] = text_buffer_init_empty();
}

static void editor_delete_character(editor_state_t *s) {
  text_buffer_t *row = &s->content.rows[s->cursor_y];
  if (row->len == 0 || s->cursor_x == (int)row->len) {
    return;
  }

  size_t n = row->len - s->cursor_x;
  memmove(&row->data[s->cursor_x], &row->data[s->cursor_x + 1], n);
  row->len--;
}

static bool editor_parse_input_normal_mode(editor_state_t *s, int k) {
  switch (k) {
  case 'h':
  case 'j':
  case 'k':
  case 'l':
  case ARROW_UP:
  case ARROW_DOWN:
  case ARROW_LEFT:
  case ARROW_RIGHT:
    editor_move_cursor(s, k);
    break;
  case 'i':
    s->mode = MODE_INSERT;
    break;
  case 'a':
    s->cursor_x++;
    if (s->cursor_x >= (int)s->content.rows[s->cursor_y].len) {
      s->cursor_x--;
    }
    s->mode = MODE_INSERT;
    break;
  case 'o':
  case 'O':
    s->cursor_y += k == 'o' ? 1 : 0;
    if (s->cursor_y < 0) {
      s->cursor_y = 0;
    }
    s->cursor_x = 0;
    editor_insert_line(s);
    s->mode = MODE_INSERT;
    break;
  case '\r':
    s->cursor_y++;
    if (s->cursor_y == (int)s->content.len) {
      s->cursor_y--;
    }
    break;
  case 'q' - 96:
    term_destroy_buffer();
    return false;
  case KEY_DELETE:
    editor_delete_character(s);
    break;
  case KEY_HOME:
    s->cursor_x = 0;
    break;
  case KEY_END:
    s->cursor_x = s->content.rows[s->cursor_y].len - 1;
    break;
  }
  return true;
}

static bool editor_parse_input_insert_mode(editor_state_t *s, int k) {
  switch (k) {
  case 27:
    s->mode = MODE_NORMAL;
    break;
  case KEY_DELETE:
    editor_delete_character(s);
    break;
  case KEY_HOME:
    s->cursor_x = 0;
    break;
  case KEY_END:
    s->cursor_x = s->content.rows[s->cursor_y].len;
    break;
  case ARROW_UP:
  case ARROW_DOWN:
  case ARROW_LEFT:
  case ARROW_RIGHT:
    editor_move_cursor(s, k);
    break;
  default:
    if (k < 1000) {
      editor_insert_character(s, (char)k);
    }
    break;
  }

  return true;
}

static bool editor_parse_input_command_mode(editor_state_t *s, int k) {
  switch (k) {
  case 27:
    s->mode = MODE_NORMAL;
    break;
  case KEY_DELETE:
    editor_delete_character(s);
    break;
  case KEY_HOME:
    s->cursor_x = 0;
    break;
  case KEY_END:
    s->cursor_x = s->content.rows[s->cursor_y].len;
    break;
  case ARROW_UP:
  case ARROW_DOWN:
  case ARROW_LEFT:
  case ARROW_RIGHT:
    editor_move_cursor(s, k);
    break;
  default:
    if (k < 1000) {
      editor_insert_character(s, (char)k);
    }
    break;
  }

  return true;
}

bool editor_parse_input(editor_state_t *s, int k) {
  bool ret = false;
  switch (s->mode) {
  case MODE_NORMAL:
    ret = editor_parse_input_normal_mode(s, k);
    break;

  case MODE_INSERT:
    ret = editor_parse_input_insert_mode(s, k);
    break;

  case MODE_COMMAND:
    ret = editor_parse_input_command_mode(s, k);
    break;
  }

  editor_scroll(s);
  return ret;
}
