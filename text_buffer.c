#include "text_buffer.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

text_buffer_t text_buffer_init_empty() {
  text_buffer_t buf;
  buf.len = 0;
  buf.capacity = 4;
  buf.data = malloc(buf.capacity * sizeof(char));

  return buf;
}

text_buffer_t text_buffer_init(const char *data, size_t len) {
  text_buffer_t buf = text_buffer_init_empty();
  text_buffer_push_str(&buf, data, len);
  return buf;
}

void text_buffer_grow(text_buffer_t *buf, size_t amount) {
  size_t new_len = buf->len + amount;
  if (new_len >= buf->capacity) {
    while (new_len >= buf->capacity) {
      buf->capacity *= 2;
    }
    buf->data = realloc(buf->data, buf->capacity * sizeof(char));
  }
  buf->len = new_len;
}

void text_buffer_push_chr(text_buffer_t *buf, char c) {
  text_buffer_grow(buf, 1);
  buf->data[buf->len - 1] = c;
}

void text_buffer_push_str(text_buffer_t *buf, const char *data, size_t len) {
  text_buffer_grow(buf, len);
  memcpy(&buf->data[buf->len - len], data, len);
}

void text_buffer_destroy(text_buffer_t *buf) { free(buf->data); }
