#ifndef _SCREEN_H_
#define _SCREEN_H_

#include "editor.h"

#define STATUS_BAR_HEIGHT 1

void screen_refresh(editor_state_t *s);

#endif
