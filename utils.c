#include "utils.h"

#include "defs.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int read_key() {
  int nread;
  char c;
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN) {
      error_and_quit("bad read");
    }
  }

  if (c == '\e') {
    if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
      return '\e';
    if (c != '[') {
      return '\e';
    }

    if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
      return '\e';
    switch (c) {
    case '2':
      if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
        return '\e';
      if (c != '~')
        return '\e';
      return KEY_INSERT;
    case '3':
      if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
        return '\e';
      if (c != '~')
        return '\e';
      return KEY_DELETE;
    case 'H':
      return KEY_HOME;
    case 'F':
      return KEY_END;
    case '5':
      if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
        return '\e';
      if (c != '~')
        return '\e';
      return KEY_PAGE_UP;
    case '6':
      if ((nread = read(STDIN_FILENO, &c, 1)) != 1)
        return '\e';
      if (c != '~')
        return '\e';
      return KEY_PAGE_DOWN;
    case 'A':
      return ARROW_UP;
    case 'B':
      return ARROW_DOWN;
    case 'C':
      return ARROW_RIGHT;
    case 'D':
      return ARROW_LEFT;
    }
  }

  return c;
}

void error_and_quit(const char *msg) {
  printf("\e[31m[error] %s\e[0m\n", msg);
  exit(1);
}
