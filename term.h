#ifndef _TERM_H_
#define _TERM_H_

#include <stdbool.h>
#include <stddef.h>

typedef struct term_buffer_t {
  char *data;
  size_t len;
  size_t capacity;
} term_buffer_t;

void term_buffer_init();
void term_push_data_to_buffer(const char *data, size_t len);
void term_write_buffer();
void term_destroy_buffer();
void term_write_and_destroy_buffer();

void term_set_normal();
void term_set_raw();
void term_clear();

void term_put_char(char c);
void term_put_string(const char *str, size_t len);

void term_move_cursor(int x, int y);
void term_move_cursor_rel(int dx, int dy);
void term_save_cursor_position();
void term_restore_cursor_position();
bool term_get_cols_rows(int *cols, int *rows);

void term_set_inverted_colors();
void term_set_bold();
void term_set_fg_color(int r, int g, int b);
void term_reset_color_style();

#endif
