#ifndef _EDITOR_H_
#define _EDITOR_H_

#include "defs.h"

#include <stdbool.h>

editor_state_t editor_init();
bool editor_parse_input(editor_state_t *s, int c);

#endif // !_EDITOR_H_
