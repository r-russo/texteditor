OBJS = te.o term.o utils.o screen.o editor.o content.o text_buffer.o
CC = gcc
CFLAGS = -Wall -Werror -Wextra -g

te: $(OBJS)
	$(CC) -o te $(OBJS) $(CFLAGS)

defs.h: $(OBJS)
term.h: term.c te.c
utils.h: term.c te.c
screen.h: te.c screen.c
content.h: content.c
editor.h: editor.c screen.c
text_buffer.h: text_buffer.c

.PHONY: clean
clean: 
	$(RM) te $(OBJS)

