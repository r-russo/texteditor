#ifndef _DEFS_H_
#define _DEFS_H_

#include <stddef.h>

typedef struct text_buffer_t {
  char *data;
  size_t len;
  size_t capacity;
} text_buffer_t;

typedef struct content_t {
  text_buffer_t *rows;
  size_t len;
  size_t capacity;
} content_t;

typedef enum editor_mode_t {
  MODE_NORMAL,
  MODE_INSERT,
  MODE_COMMAND,
} editor_mode_t;

typedef struct editor_state_t {
  editor_mode_t mode;

  int cursor_x;
  int cursor_y;
  int screen_width;
  int screen_height;

  int offset_x;
  int offset_y;
  content_t content;
  text_buffer_t command_buffer;
} editor_state_t;

typedef enum keycode_t {
  KEY_INSERT = 1000,
  KEY_DELETE,
  KEY_HOME,
  KEY_END,
  KEY_PAGE_UP,
  KEY_PAGE_DOWN,
  KEY_BACKSPACE,
  ARROW_UP,
  ARROW_DOWN,
  ARROW_LEFT,
  ARROW_RIGHT,
} keycode_t;

#endif
