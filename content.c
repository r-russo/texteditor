#include "content.h"
#include "text_buffer.h"

#include <stdlib.h>
#include <string.h>

content_t content_init_empty() {
  content_t c;
  c.len = 0;
  c.capacity = 4;
  c.rows = malloc(c.capacity * sizeof(text_buffer_t));

  return c;
}

content_t content_init(const char *data, size_t len) {
  content_t c = content_init_empty();

  size_t start = 0;
  size_t line_len = 0;
  for (size_t i = 0; i < len; i++) {
    char chr = data[i];
    if (chr == '\n') {
      if (line_len > 0) {
        char *line = malloc(line_len * sizeof(char));
        memcpy(line, &data[start], line_len);
        text_buffer_t row = text_buffer_init(line, line_len);
        content_push_row(&c, row);
        start += line_len + 1;
        line_len = 0;
        free(line);
      } else {
        content_push_row(&c, text_buffer_init_empty());
        start += line_len + 1;
        line_len = 0;
      }
    } else {
      line_len++;
    }
  }

  if (line_len > 0) {
    char *line = malloc(line_len * sizeof(char));
    memcpy(line, &data[start], line_len);
    text_buffer_t row = text_buffer_init(line, line_len);
    content_push_row(&c, row);
  }

  return c;
}

void content_grow(content_t *content) {
  size_t new_len = content->len + 1;
  if (new_len >= content->capacity) {
    while (new_len >= content->capacity) {
      content->capacity *= 2;
    }
    content->rows =
        realloc(content->rows, content->capacity * sizeof(text_buffer_t));
  }
  content->len++;
}

void content_push_row(content_t *content, text_buffer_t row) {
  content_grow(content);
  content->rows[content->len - 1] = row;
}
