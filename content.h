#ifndef _CONTENT_H_
#define _CONTENT_H_

#include "defs.h"

#include <stdbool.h>

content_t content_init_empty();
content_t content_init(const char *data, size_t len);

void content_grow(content_t *content);
void content_push_row(content_t *content, text_buffer_t row);

// size_t content_map_cursor_to_index(editor_state_t *s);
int content_map_cursor_to_index(editor_state_t *s);

#endif // !_CONTENT_H_
