#include "screen.h"

#include "term.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void screen_draw_text(editor_state_t *s) {
  for (int i = 0; i < s->screen_height - STATUS_BAR_HEIGHT; i++) {
    size_t text_row = i + s->offset_y;
    term_move_cursor(0, i);
    if (text_row < s->content.len) {
      int data_len = (int)s->content.rows[text_row].len - s->offset_x;
      if (data_len < 0) {
        data_len = 0;
      }
      term_put_string(s->content.rows[text_row].data + s->offset_x, data_len);
    } else {
      term_set_fg_color(255, 0, 0);
      term_put_char('~');
      term_reset_color_style();
    }
  }
}

static void screen_draw_status_bar(editor_state_t *s) {
  term_set_inverted_colors();

  if (s->mode != MODE_COMMAND) {
    char *line = malloc((s->screen_width + 1) * sizeof(char));
    snprintf(line, s->screen_width + 1, "%s | %d:%d", "file: [nofile]",
             s->cursor_x, s->cursor_y);
    term_move_cursor(s->screen_width - strnlen(line, s->screen_width + 1),
                     s->screen_height);
    term_put_string(line, s->screen_width + 1);

    term_reset_color_style();
    term_move_cursor(0, s->screen_height);
    char mode[32];
    switch (s->mode) {
    case MODE_NORMAL:
      snprintf(mode, 32, "NORMAL");
      term_set_fg_color(0, 255, 127);
      break;
    case MODE_INSERT:
      snprintf(mode, 32, "INSERT");
      term_set_fg_color(0, 127, 255);
      break;
    case MODE_COMMAND:
      break;
    }

    snprintf(line, s->screen_width + 1, "-- %s --", mode);
    term_set_bold();
    term_put_string(line, s->screen_width + 1);
  }

  term_reset_color_style();
}

void screen_refresh(editor_state_t *s) {
  term_buffer_init();
  term_clear();
  term_get_cols_rows(&s->screen_width, &s->screen_height);

  screen_draw_text(s);
  screen_draw_status_bar(s);
  int cx = s->cursor_x - s->offset_x;
  int cy = s->cursor_y - s->offset_y;
  term_move_cursor(cx, cy);
  term_write_and_destroy_buffer();
}
