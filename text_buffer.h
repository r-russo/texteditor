#ifndef _TEXT_BUFFER_H_
#define _TEXT_BUFFER_H_

#include "defs.h"

text_buffer_t text_buffer_init_empty();
text_buffer_t text_buffer_init(const char *data, size_t len);
void text_buffer_grow(text_buffer_t *buf, size_t amount);
void text_buffer_push_chr(text_buffer_t *buf, char c);
void text_buffer_push_str(text_buffer_t *buf, const char *data, size_t len);
void text_buffer_destroy(text_buffer_t *buf);

#endif
