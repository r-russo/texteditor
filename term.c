#include "term.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include "utils.h"

static struct termios orig_termios;
static term_buffer_t term_buffer;

void term_buffer_init() {
  term_buffer.capacity = 4;
  term_buffer.data = malloc(term_buffer.capacity * sizeof(char));
  term_buffer.len = 0;
}

void term_push_data_to_buffer(const char *data, size_t len) {
  if (term_buffer.data == NULL) {
    error_and_quit("Buffer not initialized. Call first term_buffer_init()");
  }

  size_t new_len = term_buffer.len + len;
  if (new_len >= term_buffer.capacity) {
    while (new_len >= term_buffer.capacity) {
      term_buffer.capacity *= 2;
    }
    term_buffer.data = realloc(term_buffer.data, term_buffer.capacity);
  }

  memcpy(&term_buffer.data[term_buffer.len], data, len);
  term_buffer.len += len;
}

void term_write_buffer() {
  if (term_buffer.data == NULL) {
    error_and_quit("Buffer not initialized. Call first term_buffer_init()");
  }

  write(STDOUT_FILENO, term_buffer.data, term_buffer.len);
  term_buffer.len = 0;
}

void term_destroy_buffer() {
  free(term_buffer.data);
  term_buffer.data = NULL;
  term_buffer.capacity = 0;
  term_buffer.len = 0;
}

void term_write_and_destroy_buffer() {
  term_write_buffer();
  term_destroy_buffer();
}

void term_set_normal() { tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios); }

void term_set_raw() {
  struct termios termios_p;
  tcgetattr(STDIN_FILENO, &orig_termios);
  atexit(term_set_normal);
  termios_p = orig_termios;
  termios_p.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  termios_p.c_oflag &= ~OPOST;
  termios_p.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  termios_p.c_cflag &= ~(CSIZE | PARENB);
  termios_p.c_cflag |= CS8;
  termios_p.c_cc[VMIN] = 0;
  termios_p.c_cc[VTIME] = 1;
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &termios_p) < 0) {
    error_and_quit("tcsetattr error. can't put in raw mode");
  }
}

void term_clear() { term_push_data_to_buffer("\e[2J", 4); }

void term_put_char(char c) { term_push_data_to_buffer(&c, 1); }

void term_put_string(const char *str, size_t len) {
  term_push_data_to_buffer(str, strnlen(str, len));
}

void term_move_cursor(int x, int y) {
  char buf[32];
  snprintf(buf, 32, "\e[%d;%dH", y + 1, x + 1);
  term_push_data_to_buffer(buf, strnlen(buf, 32));
}

void term_move_cursor_rel(int dx, int dy) {
  char buf[32];
  if (dx > 0) {
    snprintf(buf, 32, "\e[%dC", dx);
    term_push_data_to_buffer(buf, strnlen(buf, 32));
  } else if (dx < 0) {
    snprintf(buf, 32, "\e[%dD", -dx);
    term_push_data_to_buffer(buf, strnlen(buf, 32));
  }

  if (dy > 0) {
    snprintf(buf, 32, "\e[%dB", dy);
    term_push_data_to_buffer(buf, strnlen(buf, 32));
  } else if (dy < 0) {
    snprintf(buf, 32, "\e[%dA", -dy);
    term_push_data_to_buffer(buf, strnlen(buf, 32));
  }
}

void term_save_cursor_position() { term_push_data_to_buffer("\e7", 2); }

void term_restore_cursor_position() { term_push_data_to_buffer("\e8", 2); }

bool term_get_cols_rows(int *cols, int *rows) {
  struct winsize w;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) != 0) {
    return false;
  }
  *cols = w.ws_col;
  *rows = w.ws_row;

  return true;
}

void term_set_inverted_colors() { term_push_data_to_buffer("\e[7m", 4); }

void term_set_fg_color(int r, int g, int b) {
  char buf[32];
  snprintf(buf, 32, "\e[38;2;%d;%d;%dm", r, g, b);
  term_push_data_to_buffer(buf, strnlen(buf, 32));
}

void term_set_bold() { term_push_data_to_buffer("\e[1m", 4); }

void term_reset_color_style() { term_push_data_to_buffer("\e[0m", 4); }
